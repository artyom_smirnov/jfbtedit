Build and run
=============

::

  mvn package
  java -jar target/jfbtedit-0.0.1-SNAPSHOT-all.jar /path/to/fbt-repository

CI status
=========

.. image:: https://drone.io/bitbucket.org/artyom_smirnov/jfbtedit/status.png

https://drone.io/bitbucket.org/artyom_smirnov/jfbtedit
