/*
 * This file is part of jfbtedit.
 * Copyright (C) 2014 Artyom Smirnov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.artyomsmirnov.jfbtedit;

import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.List;
import java.util.Map;

public class TestMissingFieldsTestLoading extends Assert {
    org.artyomsmirnov.jfbtedit.Test test = new org.artyomsmirnov.jfbtedit.Test();

    @Before
    public void loadTest() throws IOException {
        Reader reader = new InputStreamReader(this.getClass().getResourceAsStream("/test2.fbt"), "UTF-8");
        BufferedReader fin = new BufferedReader(reader);

        String testBody = IOUtils.toString(fin);
        test.parse(testBody);
    }

    @Test
    public void CheckID() {
        assertEquals("some.id", test.getID());
    }

    @Test
    public void CheckQMID() {
        assertEquals(null, test.getQmid());
    }

    @Test
    public void CheckTrackerID() {
        assertNull(test.getTrackerId());
    }

    @Test
    public void CheckTitle() {
        assertEquals("Some title", test.getTitle());
    }

    @Test
    public void CheckMinVersions() {
        assertEquals("2.5", test.getMinVersions());
    }

    @Test
    public void CheckDescription()
    {
        assertNull(test.getDescription());
    }

    @Test
    public void CheckVersions()
    {
        assertEquals(2, test.getVersions().size());

        Version version0 = test.getVersions().get(0);
        assertEquals("2.1", version0.getFirebirdVersion());
        assertNull(version0.getPlatform());
        assertNull(version0.getDatabase());
        assertNull(version0.getBackup());
        assertNull(version0.getConnectionCharset());
        assertNull(version0.getDatabaseCharset());
        assertNull(version0.getPageSize());
        assertNull(version0.getInitScript());
        assertNull(version0.getTestScript());
        assertNull(version0.getExpectedStderr());

        Version version1 = test.getVersions().get(1);
        assertEquals("2.5", version1.getFirebirdVersion());
        assertEquals("All", version1.getPlatform());
        assertNull(version1.getDatabase());
        assertNull(version1.getBackup());
        assertNull(version1.getConnectionCharset());
        assertNull(version1.getDatabaseCharset());
        assertNull(version1.getPageSize());
        assertEquals("", version1.getInitScript());
        assertNull(version1.getTestScript());
        assertEquals("TEST_FNC", version1.getExpectedStderr());
    }

    @Test
    public void CheckSubstitutions()
    {
        Map<String, String> subst0 = test.getVersions().get(0).getSubstitutionsValues();
        assertEquals(0, subst0.size());

        Map<String, String> subst1 = test.getVersions().get(1).getSubstitutionsValues();
        assertEquals(0, subst1.size());
    }

    @Test
    public void CheckResources()
    {
        List<String> res0 = test.getVersions().get(0).getResources();
        assertEquals(0, res0.size());

        List<String> res1 = test.getVersions().get(1).getResources();
        assertEquals("test_user1", res1.get(0));
        assertEquals("test_user2", res1.get(1));
    }
}
