/*
 * This file is part of jfbtedit.
 * Copyright (C) 2014 Artyom Smirnov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.artyomsmirnov.jfbtedit;

import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Before;

import java.io.*;
import java.util.List;
import java.util.Map;

public class TestGUIBasicTestLoadingSaving extends Assert {
    private Test savedTest;

    @Before
    public void loadTest() throws IOException {
        Reader reader = new InputStreamReader(this.getClass().getResourceAsStream("/test1.fbt"), "UTF-8");
        BufferedReader fin = new BufferedReader(reader);

        Test originalTest = new Test();
        originalTest.parse(IOUtils.toString(fin));

        Test testFromEditor = new Editor(originalTest).getTest();

        savedTest = new Test();
        savedTest.parse(testFromEditor.toString());
    }

    @org.junit.Test
    public void CheckID() {
        assertEquals("security.ddl_access.create.function.grant-yes_revoke-no_role-no_any-no_owner-no_gropt-yes", savedTest.getID());
    }

    @org.junit.Test
    public void CheckQMID() {
        assertEquals("security.ddl_access.create.function.grant-yes_revoke-no_role-no_any-no_owner-no_gropt-yes", savedTest.getQmid());
    }

    @org.junit.Test
    public void CheckTrackerID() {
        assertEquals("Some tracker id", savedTest.getTrackerId());
    }

    @org.junit.Test
    public void CheckTicle() {
        assertEquals("Some title", savedTest.getTitle());
    }

    @org.junit.Test
    public void CheckMinVersions() {
        assertEquals("2.5", savedTest.getMinVersions());
    }

    @org.junit.Test
    public void CheckDescription()
    {
        assertEquals(
                "Передача прав на создание функций\n" +
                        "Создается база от SYSDBA.\n" +
                        "Создаются пользователи user1, user2.\n" +
                        "User1 получает права на создание функций с правом передачи.\n" +
                        "User2 получает права от user1 на создание функций.\n" +
                        "User2 соеденяется с базой и пытается создать функцию.\n" +
                        "Проверяется наличие функции.\n" +
                        "Пользователи user1, user2 удаляются.\n" +
                        "База удаляется.",
                savedTest.getDescription()
        );
    }

    @org.junit.Test
    public void CheckVersions()
    {
        assertEquals(2, savedTest.getVersions().size());

        Version version0 = savedTest.getVersions().get(0);
        assertEquals("2.1", version0.getFirebirdVersion());
        assertEquals("All", version0.getPlatform());
        assertEquals("db.fdb", version0.getDatabase());
        assertEquals("bak.fbk", version0.getBackup());
        assertEquals("CP1251", version0.getConnectionCharset());
        assertEquals("CP1251", version0.getDatabaseCharset());
        assertEquals("16000", version0.getPageSize());
        assertEquals("--foo", version0.getInitScript());
        assertEquals("#----create database----#\n" +
                "script = \"\"\"\n" +
                "CONNECT '%s';\n" +
                "GRANT CREATE FUNCTION TO \"user1\" WITH GRANT OPTION;\n" +
                "\"\"\"% dsn\n" +
                "runProgram('isql',['-user', user_name,'-password', user_password,'-q'],script)\n" +
                "\n" +
                "#----grant create function----#\n" +
                "script = \"\"\"\n" +
                "CONNECT '%s';\n" +
                "GRANT CREATE FUNCTION TO \"user2\";\n" +
                "\"\"\" %dsn\n" +
                "runProgram('isql',['-user','user1','-password','user1','-q'],script)\n" +
                "\n" +
                "#----create function----#\n" +
                "script = \"\"\"\n" +
                "CONNECT '%s';\n" +
                "DECLARE EXTERNAL FUNCTION TEST_FNC\n" +
                "RETURNS INTEGER BY VALUE\n" +
                "ENTRY_POINT 'TEST_FNC' MODULE_NAME 'system_module';\n" +
                "show functions;\n" +
                "\"\"\" %dsn\n" +
                "runProgram('isql',['-user','user2','-password','user2','-q'],script)", version0.getTestScript());
        assertEquals("--bar", version0.getExpectedStderr());

        Version version1 = savedTest.getVersions().get(1);
        assertEquals("2.5", version1.getFirebirdVersion());
        assertEquals("All", version1.getPlatform());
        assertEquals("db.fdb", version1.getDatabase());
        assertEquals("bak.fbk", version1.getBackup());
        assertEquals("UTF8", version1.getConnectionCharset());
        assertEquals("UTF8", version1.getDatabaseCharset());
        assertEquals("4096", version1.getPageSize());
        assertEquals("--some script", version1.getInitScript());
        assertEquals("#some 2.5 test\n" +
                "#----create database----#\n" +
                "script = \"\"\"\n" +
                "CONNECT '%s';\n" +
                "GRANT CREATE FUNCTION TO \"user1\" WITH GRANT OPTION;\n" +
                "\"\"\"% dsn\n" +
                "runProgram('isql',['-user', user_name,'-password', user_password,'-q'],script)\n" +
                "\n" +
                "#----grant create function----#\n" +
                "script = \"\"\"\n" +
                "CONNECT '%s';\n" +
                "GRANT CREATE FUNCTION TO \"user2\";\n" +
                "\"\"\" %dsn\n" +
                "runProgram('isql',['-user','user1','-password','user1','-q'],script)\n" +
                "\n" +
                "#----create function----#\n" +
                "script = \"\"\"\n" +
                "CONNECT '%s';\n" +
                "DECLARE EXTERNAL FUNCTION TEST_FNC\n" +
                "RETURNS INTEGER BY VALUE\n" +
                "ENTRY_POINT 'TEST_FNC' MODULE_NAME 'system_module';\n" +
                "show functions;\n" +
                "\"\"\" %dsn\n" +
                "runProgram('isql',['-user','user2','-password','user2','-q'],script)", version1.getTestScript());
        assertEquals("--blah", version1.getExpectedStderr());
    }

    @org.junit.Test
    public void CheckSubstitutions()
    {
        Map<String, String> subst = savedTest.getVersions().get(0).getSubstitutionsValues();
        assertEquals(3, subst.size());
        assertEquals("", subst.get("SQL>"));
        assertEquals("", subst.get("CON>"));
        assertEquals("", subst.get("Database:  '.*', User: .*[\\s$]"));

        Map<String, String> subst1 = savedTest.getVersions().get(1).getSubstitutionsValues();
        assertEquals(1, subst1.size());
        assertEquals("bar", subst1.get("foo"));
    }

    @org.junit.Test
    public void CheckResources()
    {
        List<String> res = savedTest.getVersions().get(0).getResources();
        assertEquals(2, res.size());
        assertEquals("test_user1", res.get(0));
        assertEquals("test_user2", res.get(1));

        List<String> res1 = savedTest.getVersions().get(1).getResources();
        assertEquals(1, res1.size());
        assertEquals("test_user2", res1.get(0));
    }
}
