/*
 * This file is part of jfbtedit.
 * Copyright (C) 2014 Artyom Smirnov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.artyomsmirnov.jfbtedit;

import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Before;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.List;
import java.util.Map;

public class TestGUIMissingFieldsTestLoadingSaving extends Assert {
    private Test savedTest;

    @Before
    public void loadTest() throws IOException {
        Reader reader = new InputStreamReader(this.getClass().getResourceAsStream("/test2.fbt"), "UTF-8");
        BufferedReader fin = new BufferedReader(reader);

        Test originalTest = new Test();
        originalTest.parse(IOUtils.toString(fin));

        Test testFromEditor = new Editor(originalTest).getTest();

        savedTest = new Test();
        savedTest.parse(testFromEditor.toString());
    }

    @org.junit.Test
    public void CheckID() {
        assertEquals("some.id", savedTest.getID());
    }

    @org.junit.Test
    public void CheckQMID() {
        assertNull(savedTest.getQmid());
    }

    @org.junit.Test
    public void CheckTrackerID() {
        assertNull(savedTest.getTrackerId());
    }

    @org.junit.Test
    public void CheckTicle() {
        assertEquals("Some title", savedTest.getTitle());
    }

    @org.junit.Test
    public void CheckMinVersions() {
        assertEquals("2.5", savedTest.getMinVersions());
    }

    @org.junit.Test
    public void CheckDescription()
    {
        assertNull(savedTest.getDescription());
    }

    @org.junit.Test
    public void CheckVersions()
    {
        assertEquals(2, savedTest.getVersions().size());

        Version version0 = savedTest.getVersions().get(0);

        assertEquals("2.1", version0.getFirebirdVersion());
        assertNull(version0.getPlatform());
        assertNull(version0.getDatabase());
        assertNull(version0.getBackup());
        assertNull(version0.getConnectionCharset());
        assertNull(version0.getDatabaseCharset());
        assertNull(version0.getPageSize());
        assertNull(version0.getInitScript());
        assertNull(version0.getTestScript());
        assertNull(version0.getExpectedStderr());

        Version version1 = savedTest.getVersions().get(1);
        assertEquals("2.5", version1.getFirebirdVersion());
        assertEquals("All", version1.getPlatform());
        assertNull(version1.getDatabase());
        assertNull(version1.getBackup());
        assertNull(version1.getConnectionCharset());
        assertNull(version1.getDatabaseCharset());
        assertNull(version1.getPageSize());
        assertEquals(null, version1.getInitScript());
        assertNull(version1.getTestScript());
        assertEquals("TEST_FNC", version1.getExpectedStderr());
    }

    @org.junit.Test
    public void CheckSubstitutions()
    {
        Map<String, String> subst0 = savedTest.getVersions().get(0).getSubstitutionsValues();
        assertEquals(0, subst0.size());

        Map<String, String> subst1 = savedTest.getVersions().get(1).getSubstitutionsValues();
        assertEquals(0, subst1.size());
    }

    @org.junit.Test
    public void CheckResources()
    {
        List<String> res0 = savedTest.getVersions().get(0).getResources();
        assertEquals(0, res0.size());

        List<String> res1 = savedTest.getVersions().get(1).getResources();
        assertEquals(2, res1.size());
        assertEquals("test_user1", res1.get(0));
        assertEquals("test_user2", res1.get(1));
    }
}
