/*
* This file is part of jfbtedit.
* Copyright (C) 2014 Artyom Smirnov
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package org.artyomsmirnov.jfbtedit;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;

import javax.swing.*;
import javax.swing.tree.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.*;
import java.util.Arrays;

import static org.artyomsmirnov.jfbtedit.ExplorerCellObject.CellTreeType.TEST_CASE;
import static org.artyomsmirnov.jfbtedit.ExplorerCellObject.CellTreeType.TEST_ROOT;
import static org.artyomsmirnov.jfbtedit.ExplorerCellObject.CellTreeType.TEST_SUITE;

public class Explorer extends JFrame {
    private final JTree testsTree;

    public Explorer(String root) {
        super("JFBTEdit");
        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new BorderLayout());

        testsTree = new JTree();
        JScrollPane scrollPane = new JScrollPane(testsTree);
        mainPanel.add(scrollPane, BorderLayout.CENTER);

        setContentPane(mainPanel);
        fillTree(root);

        testsTree.addMouseListener(new TestsTreeMouseAdapter());

        pack();
        //setLocationRelativeTo(null);
        setLocationByPlatform(true);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setVisible(true);
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(300, 500);
    }

    private void fillTree(String root) {
        String testsDir = Utils.pathJoin(root, "tests");
        DefaultMutableTreeNode rootNode = new DefaultMutableTreeNode("");
        DefaultMutableTreeNode testsNode = new DefaultMutableTreeNode(new ExplorerCellObject("Tests", ExplorerCellObject.CellTreeType.TEST_ROOT, null, testsDir));
        DefaultMutableTreeNode resourcesNode = new DefaultMutableTreeNode("Resources");
        DefaultMutableTreeNode fbkNode = new DefaultMutableTreeNode("FBK");
        DefaultMutableTreeNode filesNode = new DefaultMutableTreeNode("Files");
        rootNode.add(testsNode);
        rootNode.add(resourcesNode);
        rootNode.add(fbkNode);
        rootNode.add(filesNode);

        fillTests(testsDir, "", testsNode);

        DefaultTreeModel TreeModel = new DefaultTreeModel(rootNode);
        testsTree.setModel(TreeModel);
        testsTree.setRootVisible(false);
        testsTree.setShowsRootHandles(true);
        testsTree.setCellRenderer(new CellRenderer());
    }

    private void fillTests(String dir, String currentID, DefaultMutableTreeNode root) {
        File rootFolder = new File(dir);
        File[] list = rootFolder.listFiles();
        if (list != null) {
            Arrays.sort(list);
            for (File f : list) {
                ExplorerCellObject.CellTreeType type;
                if (f.isFile() && FilenameUtils.getExtension(f.getName()).equals("fbt"))
                    type = ExplorerCellObject.CellTreeType.TEST_CASE;
                else if (f.isDirectory())
                    type = ExplorerCellObject.CellTreeType.TEST_SUITE;
                else
                    continue;

                String baseName = FilenameUtils.getBaseName(f.getName());
                String newId = (currentID.equals("") ?  baseName: currentID + "." + baseName);
                DefaultMutableTreeNode subRoot = new DefaultMutableTreeNode(
                        new ExplorerCellObject(baseName, type, newId, f.getAbsolutePath()));
                if (f.isDirectory()) {
                    fillTests(f.getAbsolutePath(), newId, subRoot);
                }

                root.add(subRoot);
            }
        }
    }

    private static class CellRenderer extends DefaultTreeCellRenderer {
        @Override
        public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
            super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);

            if (value instanceof DefaultMutableTreeNode) {
                DefaultMutableTreeNode node = (DefaultMutableTreeNode) value;
                if (node.getUserObject() instanceof ExplorerCellObject) {
                    ExplorerCellObject cellObject = (ExplorerCellObject) node.getUserObject();
                    setText(cellObject.getCaption());
                    setIcon(cellObject.getIcon());
                }
            }

            return this;
        }

    }

    private class TestsTreeMouseAdapter extends MouseAdapter {
        @Override
        public void mousePressed(MouseEvent e) {
            TreePath selPath = testsTree.getPathForLocation(e.getX(), e.getY());
            if (selPath == null)
                return;
            final DefaultMutableTreeNode node = (DefaultMutableTreeNode) selPath.getLastPathComponent();
            Object userObject = node.getUserObject();
            if (!(userObject instanceof ExplorerCellObject))
                return;
            final ExplorerCellObject cellObject = (ExplorerCellObject) userObject;
            switch (e.getButton()) {
                case MouseEvent.BUTTON1: {
                    if (e.getClickCount() == 2) {
                        if (cellObject.getType() == TEST_CASE) {
                            try {
                                final String testFileName = cellObject.getPath();
                                final Test test = new Test();

                                Reader reader = new InputStreamReader(
                                        new FileInputStream(testFileName), "UTF-8");
                                BufferedReader fin = new BufferedReader(reader);

                                String testBody = IOUtils.toString(fin);
                                test.parse(testBody);

                                final Editor editor = new Editor(test);
                                editor.addSaveListener(new TestEditorSaveEvent(testFileName));
                                editor.setVisible(true);
                            } catch (Exception ex) {
                                String msg = String.format("Unable to load test");
                                JOptionPane.showMessageDialog(Explorer.this, msg, "Error", JOptionPane.PLAIN_MESSAGE);
                            }
                        }

                    }
                    break;
                }

                case MouseEvent.BUTTON3: {
                    JPopupMenu menu = new JPopupMenu();
                    JMenuItem newTest = new JMenuItem("New test");
                    newTest.addActionListener(new NewTestActionListener(cellObject, node));
                    menu.add(newTest);

                    JMenuItem newSuite = new JMenuItem("New suite");
                    newSuite.addActionListener(new NewSuiteActionListener(cellObject));
                    menu.add(newSuite);

                    JMenuItem delete = new JMenuItem("Delete");
                    delete.addActionListener(new DeleteActionListener(cellObject, node));
                    menu.add(delete);

                    Rectangle pathBounds = testsTree.getUI().getPathBounds(testsTree, selPath);
                    menu.show(testsTree, pathBounds.x, pathBounds.y + pathBounds.height);
                    break;
                }
            }
        }

        private class NewTestActionListener implements ActionListener {
            private final ExplorerCellObject cellObject;
            private final DefaultMutableTreeNode node;

            public NewTestActionListener(ExplorerCellObject cellObject, DefaultMutableTreeNode node) {
                this.cellObject = cellObject;
                this.node = node;
            }

            @Override
            public void actionPerformed(ActionEvent e) {
                String basePath;
                String baseId;
                final DefaultMutableTreeNode parentNode;
                if (cellObject.getType() == TEST_ROOT)
                {
                    basePath = cellObject.getPath();
                    baseId = "";
                    parentNode = node;
                }
                else if (cellObject.getType() == TEST_SUITE)
                {
                    basePath = cellObject.getPath();
                    baseId = cellObject.getID();
                    parentNode = node;
                }
                else if (cellObject.getType() == TEST_CASE)
                {
                    ExplorerCellObject parentUserObject = (ExplorerCellObject)((DefaultMutableTreeNode) node.getParent()).getUserObject();
                    basePath = parentUserObject.getPath();
                    baseId = parentUserObject.getID();
                    parentNode = (DefaultMutableTreeNode) node.getParent();
                }
                else
                {
                    return;
                }

                CreateDialog dialog = new CreateDialog(baseId.equals("") ? "root of tests":baseId, false);
                dialog.setVisible(true);
                final String newItem = dialog.getResult();
                if (newItem == null)
                    return;

                final String newFile = Utils.pathJoin(basePath, newItem) + ".fbt";
                final String newID = baseId.equals("") ? newItem : baseId + "." + newItem;
                if (new File(newFile).exists())
                {
                    JOptionPane.showMessageDialog(null,
                            String.format("Test %s already exists", newID),
                            "Can not create test",
                            JOptionPane.ERROR_MESSAGE);
                    return;
                }
                Test newTest = new Test();
                newTest.setId(newID);
                newTest.setQmid(newID);

                Editor editor = new Editor(newTest);
                editor.addSaveListener(new NewTestEditorSaveEvent(newFile, newItem, newID, parentNode));
                editor.setVisible(true);
            }

            private class NewTestEditorSaveEvent implements EditorSaveEvent {
                private final String newFile;
                private final String newItem;
                private final String newID;
                private final DefaultMutableTreeNode parentNode;

                public NewTestEditorSaveEvent(String newFile, String newItem, String newID, DefaultMutableTreeNode parentNode) {
                    this.newFile = newFile;
                    this.newItem = newItem;
                    this.newID = newID;
                    this.parentNode = parentNode;
                }

                @Override
                public void save(Editor editor) {
                    try {
                        boolean createNode = !(new File(newFile).exists());
                        PrintWriter writer = new PrintWriter(newFile);
                        writer.print(editor.getTest().toString());
                        writer.close();
                        if (createNode) {
                            DefaultMutableTreeNode newNode = new DefaultMutableTreeNode(
                                    new ExplorerCellObject(newItem, TEST_CASE, newID, newFile));
                            parentNode.add(newNode);
                            ((DefaultTreeModel) testsTree.getModel()).reload(parentNode);
                        }
                    } catch (FileNotFoundException e1) {
                        e1.printStackTrace();
                    }
                }
            }
        }

        private class DeleteActionListener implements ActionListener {
            private final ExplorerCellObject cellObject;
            private final DefaultMutableTreeNode node;

            public DeleteActionListener(ExplorerCellObject cellObject, DefaultMutableTreeNode node) {
                this.cellObject = cellObject;
                this.node = node;
            }

            @Override
            public void actionPerformed(ActionEvent e) {
                String test = cellObject.getPath();
                //TODO: Show warning before removing and error message
                switch (cellObject.getType()) {
                    case TEST_CASE:
                        if (new File(test).delete()) {
                            DefaultMutableTreeNode parentNode = (DefaultMutableTreeNode) node.getParent();
                            node.removeFromParent();
                            ((DefaultTreeModel) testsTree.getModel()).reload(parentNode);
                        }
                        break;
                    case TEST_SUITE:
                        try {
                            FileUtils.deleteDirectory(new File(test));
                            DefaultMutableTreeNode parentNode = (DefaultMutableTreeNode) node.getParent();
                            node.removeFromParent();
                            ((DefaultTreeModel) testsTree.getModel()).reload(parentNode);
                        } catch (IOException ignored) {
                        }
                        break;
                }
            }
        }

        private class NewSuiteActionListener implements ActionListener {
            private final ExplorerCellObject cellObject;

            public NewSuiteActionListener(ExplorerCellObject cellObject) {
                this.cellObject = cellObject;
            }

            @Override
            public void actionPerformed(ActionEvent e) {
                String suite = cellObject.getPath();
                if (cellObject.getType() == TEST_SUITE) {
                    System.out.println("Will create suite in " + suite);
                }
            }
        }

        private class TestEditorSaveEvent implements EditorSaveEvent {
            private final String testFileName;

            public TestEditorSaveEvent(String testFileName) {
                this.testFileName = testFileName;
            }

            @Override
            public void save(Editor editor) {
                try {
                    PrintWriter writer = new PrintWriter(testFileName);
                    writer.print(editor.getTest().toString());
                    writer.close();
                } catch (FileNotFoundException e1) {
                    e1.printStackTrace();
                }
            }
        }
    }
}
