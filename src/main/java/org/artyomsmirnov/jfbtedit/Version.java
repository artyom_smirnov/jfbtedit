/*
* This file is part of jfbtedit.
* Copyright (C) 2014 Artyom Smirnov
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package org.artyomsmirnov.jfbtedit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Version {
    private Map<String, Object> versionValues = new HashMap<String, Object>();

    private Map<String, String> substitutionsValues = new HashMap<String, String>();

    private List<String> resourcesValues = new ArrayList<String>();

    public void setValue(String key, Object value) {
        versionValues.put(key, value);
    }

    public Object getValue(String key) {
        return versionValues.get(key);
    }

    public boolean containsKey(String id) {
        return versionValues.containsKey(id);
    }

    public String getFirebirdVersion() {
        return (String) versionValues.get(Test.FIREBIRD_VERSION);
    }

    public void setFirebirdVersion(String firebirdVersion) {
        versionValues.put(Test.FIREBIRD_VERSION, firebirdVersion);
    }

    public String getPlatform() {
        return (String) versionValues.get(Test.PLATFORM);
    }

    public void setPlatform(String platform) {
        versionValues.put(Test.PLATFORM, platform);
    }

    public String getDatabase() {
        return (String) versionValues.get(Test.DATABASE_FILE);
    }

    public void setDatabase(String database) {
        versionValues.put(Test.DATABASE_FILE, database);
    }

    public String getBackup() {
        return (String) versionValues.get(Test.BACKUP_FILE);
    }

    public void setBackup(String backup) {
        versionValues.put(Test.BACKUP_FILE, backup);
    }

    public String getConnectionCharset() {
        return (String) versionValues.get(Test.CONNECTION_CHARSET);
    }

    public void setConnectionCharset(String connectionCharset) {
        versionValues.put(Test.CONNECTION_CHARSET, connectionCharset);
    }

    public String getDatabaseCharset() {
        return (String) versionValues.get(Test.DATABASE_CHARSET);
    }

    public void setDatabaseCharset(String connectionCharset) {
        versionValues.put(Test.DATABASE_CHARSET, connectionCharset);
    }

    public String getPageSize() {
        return (String) versionValues.get(Test.PAGE_SIZE);
    }

    public void setPageSize(String pageSize) {
        versionValues.put(Test.PAGE_SIZE, pageSize);
    }

    public String getInitScript() {
        return (String) versionValues.get(Test.INIT_SCRIPT);
    }

    public void setInitScript(String initScript) {
        versionValues.put(Test.INIT_SCRIPT, initScript);
    }

    public String getTestScript() {
        return (String) versionValues.get(Test.TEST_SCRIPT);
    }

    public void setTestScript(String testScript) {
        versionValues.put(Test.TEST_SCRIPT, testScript);
    }

    public String getTestType() {
        return (String) versionValues.get(Test.TEST_TYPE);
    }

    public void setTestType(String testType) {
        versionValues.put(Test.TEST_TYPE, testType);
    }

    public String getExpectedStdout() {
        return (String) versionValues.get(Test.EXPECTED_STDOUT);
    }

    public void setExpectedStdout(String expectedStdout) {
        versionValues.put(Test.EXPECTED_STDOUT, expectedStdout);
    }

    public String getExpectedStderr() {
        return (String) versionValues.get(Test.EXPECTED_STDERR);
    }

    public void setExpectedStderr(String expectedStderr) {
        versionValues.put(Test.EXPECTED_STDERR, expectedStderr);
    }

    public List<String> getResources() {
        return resourcesValues;
    }

    public void setSubstitution(String pattern, String value) {
        substitutionsValues.put(pattern, value);
    }

    public Map<String, String> getSubstitutionsValues() {
        return substitutionsValues;
    }

    public void addResource(String resource) {
        resourcesValues.add(resource);
    }

}