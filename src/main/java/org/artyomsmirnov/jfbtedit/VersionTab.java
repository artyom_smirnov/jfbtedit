/*
 * This file is part of jfbtedit.
 * Copyright (C) 2014 Artyom Smirnov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.artyomsmirnov.jfbtedit;

import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rsyntaxtextarea.SyntaxConstants;
import org.fife.ui.rtextarea.RTextScrollPane;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Map;
import java.util.List;

public class VersionTab extends JPanel {
    private JPanel tabPanel;
    private JTextField firebirdVersion;
    private JTextField platform;
    private JTextField database;
    private JTextField backup;
    private JTextField connectionCharset;
    private JTextField databaseCharset;
    private JTextField pageSize;
    private JCheckBox enableInitScript;
    private RSyntaxTextArea initScript;
    private JCheckBox enableTestScript;
    private JComboBox testType;
    private RSyntaxTextArea testScript;
    private JCheckBox enableExpectedStdout;
    private RSyntaxTextArea expectedStdout;
    private JCheckBox enableExpectedStderr;
    private RSyntaxTextArea expectedStderr;
    private JTable substitutions;
    private JTable resources;
    private JButton addSubstitution;
    private JButton delSubstitution;
    private JButton addResource;
    private JButton delResource;
    private RTextScrollPane initScriptScroll;
    private RTextScrollPane testScriptScroll;
    private RTextScrollPane expectedStdoutScroll;
    private RTextScrollPane expectedStderrScroll;
    private List<VersionListener> versionListeners = new ArrayList<VersionListener>();

    public VersionTab(Version version) {
        super();
        this.setLayout(new BorderLayout());
        this.add(tabPanel, BorderLayout.CENTER);

        firebirdVersion.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                callListeners();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                callListeners();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                callListeners();
            }

            private void callListeners()
            {
                for (VersionListener listener: versionListeners)
                    listener.newVersion(firebirdVersion.getText());
            }
        });

        initScript.getDocument().addDocumentListener(new FormUpdater(this));
        initScript.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_SQL);
        initScriptScroll.setLineNumbersEnabled(true);
        enableInitScript.addItemListener(new ComponentHider(this, initScriptScroll));
        testScript.getDocument().addDocumentListener(new FormUpdater(this));
        testScript.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_SQL);
        testScriptScroll.setLineNumbersEnabled(true);
        enableTestScript.addItemListener(new ComponentHider(this, testScriptScroll));
        enableTestScript.addItemListener(new ComponentHider(this, testType));
        expectedStdout.getDocument().addDocumentListener(new FormUpdater(this));
        expectedStdoutScroll.setLineNumbersEnabled(true);
        enableExpectedStdout.addItemListener(new ComponentHider(this, expectedStdoutScroll));
        expectedStderr.getDocument().addDocumentListener(new FormUpdater(this));
        expectedStdoutScroll.setLineNumbersEnabled(true);
        enableExpectedStderr.addItemListener(new ComponentHider(this, expectedStderrScroll));
        substitutions.setModel(new DefaultTableModel(new String[]{"Pattern", "Substitution"}, 0));
        substitutions.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        resources.setModel(new DefaultTableModel(new String[]{"Resource"}, 0));
        resources.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        testType.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                switch (testType.getSelectedIndex()) {
                    default:
                    case 0:
                        testScript.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_SQL);
                        break;
                    case 1:
                        testScript.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_PYTHON);
                        break;
                }
            }
        });

        //Add substitution button
        addSubstitution.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ((DefaultTableModel)substitutions.getModel()).addRow(new Object[]{"", ""});
            }
        });
        //Del substitution button
        delSubstitution.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ((DefaultTableModel)substitutions.getModel()).removeRow(substitutions.getSelectedRow());
            }
        });

        //Add resource button
        addResource.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ((DefaultTableModel)resources.getModel()).addRow(new Object[]{""});
            }
        });
        delResource.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ((DefaultTableModel)resources.getModel()).removeRow(resources.getSelectedRow());
            }
        });

        //Load fields
        if (version != null) {
            firebirdVersion.setText(version.getFirebirdVersion());
            platform.setText(version.getPlatform());
            database.setText(version.getDatabase());
            backup.setText(version.getBackup());
            connectionCharset.setText(version.getConnectionCharset());
            databaseCharset.setText(version.getDatabaseCharset());
            pageSize.setText(version.getPageSize());

            String initScriptStr = version.getInitScript();
            if (initScriptStr != null &&initScriptStr.length() != 0) {
                enableInitScript.setSelected(true);
                initScript.setText(initScriptStr);
            } else {
                enableInitScript.setSelected(false);
            }

            String testScriptStr = version.getTestScript();
            if (testScriptStr != null && testScriptStr.length() != 0) {
                enableTestScript.setSelected(true);
                testScript.setText(testScriptStr);
            } else {
                enableTestScript.setSelected(false);
            }

            String testTypeStr = version.getTestType();
            if (testTypeStr == null || testTypeStr.equals("ISQL"))
            {
                testType.setSelectedIndex(0);
            }
            else
            {
                testType.setSelectedIndex(1);
            }

            String expectedStdoutStr = version.getExpectedStdout();
            if (expectedStdoutStr != null && expectedStdoutStr.length() != 0) {
                enableExpectedStdout.setSelected(true);
                expectedStdout.setText(expectedStdoutStr);
            } else {
                enableExpectedStdout.setSelected(false);
            }

            String expectedStderrStr = version.getExpectedStderr();
            if (expectedStderrStr != null && expectedStderrStr.length() != 0) {
                enableExpectedStderr.setSelected(true);
                expectedStderr.setText(expectedStderrStr);
            } else {
                enableExpectedStderr.setSelected(false);
            }

            DefaultTableModel substitutionsModel = (DefaultTableModel) substitutions.getModel();
            for (Map.Entry<String, String> s : version.getSubstitutionsValues().entrySet()) {
                substitutionsModel.addRow(new Object[]{s.getKey(), s.getValue()});
            }

            DefaultTableModel resourcesModel = (DefaultTableModel) resources.getModel();
            for (String s : version.getResources()) {
                resourcesModel.addRow(new Object[]{s});
            }
        }
    }

    public Version getVersion() {
        Version version = new Version();

        version.setFirebirdVersion(firebirdVersion.getText());
        version.setPlatform(platform.getText());
        version.setDatabase(database.getText());
        version.setBackup(backup.getText());
        version.setConnectionCharset(connectionCharset.getText());
        version.setDatabaseCharset(databaseCharset.getText());
        version.setPageSize(pageSize.getText());
        if (enableInitScript.isSelected()) {
            version.setInitScript(initScript.getText());
        }
        if (enableTestScript.isSelected()) {
            version.setTestScript(testScript.getText());
            version.setTestType((String)testType.getModel().getSelectedItem());
        }
        if (enableExpectedStdout.isSelected()) {
            version.setExpectedStdout(expectedStdout.getText());
        }
        if (enableExpectedStderr.isSelected()) {
            version.setExpectedStderr(expectedStderr.getText());
        }
        DefaultTableModel substitutionsModel = (DefaultTableModel) substitutions.getModel();
        for (int i = 0; i < substitutionsModel.getRowCount(); i++) {
            version.setSubstitution((String)substitutionsModel.getValueAt(i, 0), (String)substitutionsModel.getValueAt(i, 1));
        }
        DefaultTableModel resourcesModel = (DefaultTableModel) resources.getModel();
        for (int i = 0; i < resourcesModel.getRowCount(); i++) {
            version.addResource((String)resourcesModel.getValueAt(i, 0));
        }

        return version;
    }

    public void addVersionListener(VersionListener listener)
    {
        versionListeners.add(listener);
    }

    public interface VersionListener
    {
        void newVersion(String version);
    }
}
