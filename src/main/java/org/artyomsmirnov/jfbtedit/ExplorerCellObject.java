/*
 * This file is part of jfbtedit.
 * Copyright (C) 2014 Artyom Smirnov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.artyomsmirnov.jfbtedit;

import javax.swing.*;

public class ExplorerCellObject {
    private final String caption;
    private final CellTreeType type;
    private final String id;
    private final String path;

    public ExplorerCellObject(String caption, CellTreeType type, String id, String path) {
        this.caption = caption;
        this.type = type;
        this.id = id;
        this.path = path;
    }

    public String getCaption() {
        return caption;
    }

    public Icon getIcon() {
        switch (type) {
            case TEST_ROOT:
            case TEST_SUITE:
                return UIManager.getIcon("Tree.openIcon");

            case TEST_CASE:
                return UIManager.getIcon("Tree.leafIcon");

            default:
                return UIManager.getIcon("Tree.leafIcon");
        }
    }

    public String getID() {
        return id;
    }

    public String getPath() {
        return path;
    }

    public CellTreeType getType() {
        return type;
    }

    enum CellTreeType {
        UNKNOWN,
        TEST_ROOT,
        TEST_SUITE,
        TEST_CASE,
    }
}
