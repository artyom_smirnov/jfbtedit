/*
* This file is part of jfbtedit.
* Copyright (C) 2014 Artyom Smirnov
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package org.artyomsmirnov.jfbtedit;

import org.python.core.PyDictionary;
import org.python.core.PyString;
import org.python.util.PythonInterpreter;

import java.io.UnsupportedEncodingException;
import java.util.*;

/**
 * FBT test is piece of python code. To be precise it python dictionary. We simply feed it
 * to Jython interpreter and get any values of interest
 */
public class Test {
    public static final String ID = "id";
    public static final String QMID = "qmid";
    public static final String TRACKER_ID = "tracker_id";
    public static final String TITLE = "title";
    public static final String DESCRIPTION = "description";
    public static final String MIN_VERSIONS = "min_versions";
    private final String[] generalIDS = {ID, QMID, TRACKER_ID, TITLE, DESCRIPTION, MIN_VERSIONS};
    public static final String VERSIONS = "versions";
    public static final String FIREBIRD_VERSION = "firebird_version";
    public static final String PLATFORM = "platform";
    public static final String DATABASE_FILE = "database";
    public static final String BACKUP_FILE = "backup_file";
    public static final String SQL_DIALECT = "sql_dialect";
    public static final String CONNECTION_CHARSET = "connection_character_set";
    public static final String DATABASE_CHARSET = "database_character_set";
    public static final String PAGE_SIZE = "page_size";
    public static final String INIT_SCRIPT = "init_script";
    public static final String TEST_TYPE = "test_type";
    public static final String TEST_SCRIPT = "test_script";
    public static final String EXPECTED_STDOUT = "expected_stdout";
    public static final String EXPECTED_STDERR = "expected_stderr";
    private final String[] versionIDS = {
            FIREBIRD_VERSION, PLATFORM, DATABASE_FILE, BACKUP_FILE, SQL_DIALECT, CONNECTION_CHARSET, DATABASE_CHARSET,
            PAGE_SIZE, INIT_SCRIPT, TEST_TYPE, TEST_SCRIPT, EXPECTED_STDOUT, EXPECTED_STDERR};
    private final String[] multistring = {DESCRIPTION, INIT_SCRIPT, TEST_SCRIPT, EXPECTED_STDOUT, EXPECTED_STDERR};
    public static final String SUBSTITUTIONS = "substitutions";
    public static final String RESOURCES = "resources";
    private Map<String, Object> generalValues = new HashMap<String, Object>();
    private List<Version> versionValues = new ArrayList<Version>();

    public Test() {
    }

    public void parse(String testBody) throws UnsupportedEncodingException {
        PythonInterpreter interpreter = new PythonInterpreter();
        PyDictionary dic = (PyDictionary) interpreter.eval(new String(testBody.getBytes("UTF-8"), "ISO-8859-1"));

        for (String id : generalIDS) {
            if (dic.has_key(new PyString(id))) {
                Object o = dic.get(id);
                if (o == null)
                    continue;

                if (o instanceof String) {
                    String val = new String(((String) o).getBytes("ISO-8859-1"), "UTF-8");
                    generalValues.put(id, val);
                } else {
                    generalValues.put(id, o);
                }
            }
        }

        if (dic.has_key(new PyString(VERSIONS))) {
            List<PyDictionary> versions = (List<PyDictionary>) dic.get(VERSIONS);
            for (PyDictionary version : versions) {
                Version versionValue = new Version();
                for (String id : versionIDS) {
                    if (version.has_key(new PyString(id))) {
                        Object o = version.get(id);
                        if (o == null)
                            continue;

                        if (o instanceof String) {
                            String val = new String(((String) o).getBytes("ISO-8859-1"), "UTF-8");
                            versionValue.setValue(id, val);
                        } else {
                            versionValue.setValue(id, o);
                        }
                    }
                }

                if (version.has_key(new PyString(SUBSTITUTIONS))) {
                    List<List<String>> substitutions = (List<List<String>>) version.get(SUBSTITUTIONS);
                    for (List<String> substitution : substitutions) {
                        versionValue.setSubstitution(substitution.get(0), substitution.get(1));
                    }
                }

                if (version.has_key(new PyString(RESOURCES))) {
                    List<String> resources = (List<String>) version.get(RESOURCES);
                    for (String resource : resources) {
                        versionValue.addResource(resource);
                    }
                }

                versionValues.add(versionValue);
            }
        }
    }

    public String toString() {
        String s;
        s = "{\n";
        for (String id : generalIDS) {
            if (generalValues.containsKey(id)) {
                Object val = generalValues.get(id);
                if (val == null) {
                    s += String.format("'%s': None,\n", id);
                } else {
                    if (val instanceof String) {
                        String strVal = (String) val;
                        if (strVal.isEmpty())
                            continue;
                        if (Arrays.asList(multistring).contains(id)) {
                            s += String.format("'%s': '''%s''',\n", id, escapeMultiString(strVal));
                        } else {
                            s += String.format("'%s': '%s',\n", id, escapeString(strVal));
                        }
                    } else {
                        s += String.format("'%s': %s,\n", id, val);
                    }
                }
            }
        }

        s += "'versions': [\n";
        boolean firstVersion = true;
        for (Version version : versionValues) {
            if (!firstVersion)
                s += ",\n";
            firstVersion = false;

            s += "{\n";
            boolean first = true;
            for (String id : versionIDS) {
                if (version.containsKey(id)) {
                    Object val = version.getValue(id);
                    if (val == null) {
                        if (!first)
                            s += ",\n";
                        s += String.format(" '%s': None", id);
                    } else {
                        if (val instanceof String) {
                            String strVal = (String) val;
                            if (strVal.isEmpty()) {
                                continue;
                            }
                            if (!first)
                                s += ",\n";
                            if (Arrays.asList(multistring).contains(id)) {
                                s += String.format(" '%s': '''%s'''", id, escapeMultiString(strVal));
                            } else {
                                s += String.format(" '%s': '%s'", id, escapeString(strVal));
                            }
                        } else {
                            if (!first)
                                s += ",\n";
                            s += String.format(" '%s': %s", id, val);
                        }
                    }
                    first = false;
                }
            }

            if (version.getResources().size() > 0) {
                s += ",\n";
                s += String.format(" '%s': [", RESOURCES);
                first = true;
                for (String resource : version.getResources()) {
                    if (!first)
                        s += ",";
                    first = false;
                    s += String.format("'%s'", escapeString(resource));
                }
                s += "]";
            }

            if (version.getSubstitutionsValues().size() > 0) {
                s += ",\n";
                s += String.format(" '%s': [", SUBSTITUTIONS);
                first = true;
                for (Map.Entry<String, String> substitution : version.getSubstitutionsValues().entrySet()) {
                    if (!first)
                        s += ",";
                    first = false;
                    s += String.format("('%s','%s')", escapeString(substitution.getKey()), escapeString(substitution.getValue()));
                }
                s += "]";
            }

            s += "\n}";
        }
        s += "\n]\n}\n";
        return s;
    }

    public String getID() {
        return (String) generalValues.get(ID);
    }

    public String getQmid() {
        return (String) generalValues.get(QMID);
    }

    public void setQmid(String qmid) {
        generalValues.put(QMID, qmid);
    }

    public String getTrackerId() {
        return (String) generalValues.get(TRACKER_ID);
    }

    public void setTrackerId(String trackerId) {
        generalValues.put(TRACKER_ID, trackerId);
    }

    public String getMinVersions() {
        return (String) generalValues.get(MIN_VERSIONS);
    }

    public void setMinVersions(String minVersions) {
        generalValues.put(MIN_VERSIONS, minVersions);
    }

    public String getTitle() {
        return (String) generalValues.get(TITLE);
    }

    public void setTitle(String title) {
        generalValues.put(TITLE, title);
    }

    public String getDescription() {
        return (String) generalValues.get(DESCRIPTION);
    }

    public void setDescription(String description) {
        generalValues.put(DESCRIPTION, description);
    }

    public void setId(String id) {
        generalValues.put(ID, id);
    }

    private String escapeString(String s) {
        s = s.replace("\'", "\\'");
        s = s.replace("\t", "\\t");
        return s.replace("\n", "\\n");
    }

    private String escapeMultiString(String s) {
        s = s.replace("'''", "\\'\\'\\'");
        return s;
    }

    public List<Version> getVersions() {
        return versionValues;
    }

    public void addVersion(Version version) {
        versionValues.add(version);
    }
}

