/*
 * This file is part of jfbtedit.
 * Copyright (C) 2014 Artyom Smirnov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.artyomsmirnov.jfbtedit;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

public class Editor extends JFrame {
    private JPanel rootPane;
    private JTextField id;
    private JTextField qmid;
    private JTextField trackerID;
    private JTextField testTitle;
    private JTextArea description;
    private JTextField minVersions;
    private JTabbedPane versionTabs;
    private JButton saveButton;
    private List<EditorSaveEvent> saveListeners = new ArrayList<EditorSaveEvent>();

    public Editor(Test test)
    {
        super();
        setContentPane(rootPane);

        //Save button
        saveButton.setIcon(UIManager.getIcon("FileView.floppyDriveIcon"));
        saveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                for(EditorSaveEvent event: saveListeners)
                    event.save(Editor.this);
            }
        });

        //Update form when description edited
        description.getDocument().addDocumentListener(new FormUpdater(this));

        //Update form title
        id.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                setTitle(id.getText());
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                setTitle(id.getText());
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                setTitle(id.getText());
            }
        });

        //Load fields
        if (test != null)
        {
            id.setText(test.getID());
            qmid.setText(test.getQmid());
            trackerID.setText(test.getTrackerId());
            testTitle.setText(test.getTitle());
            description.setText(test.getDescription());
            minVersions.setText(test.getMinVersions());

            for(Version version: test.getVersions())
            {
                final VersionTab tab = new VersionTab(version);
                versionTabs.add(version.getFirebirdVersion(), tab);
                setTabTitleUpdated(tab);
            }
        }

        //Tab for creating new versions
        versionTabs.addTab("+", new JPanel());
        versionTabs.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (versionTabs.getSelectedIndex() == versionTabs.getTabCount() - 1) {
                    VersionTab tab = new VersionTab(null);
                    versionTabs.insertTab("", null, tab, null, versionTabs.getTabCount() - 1);
                    versionTabs.setSelectedIndex(versionTabs.getTabCount() - 2);
                    setTabTitleUpdated(tab);
                } else {
                    if (e.getButton() == 2) {
                        int i = versionTabs.getSelectedIndex();
                        versionTabs.remove(i);
                    }
                }
            }
        });

        doLayout();
        pack();

        setLocationByPlatform(true);
    }

    public Test getTest()
    {
        Test test = new Test();

        test.setId(id.getText());
        test.setQmid(qmid.getText());
        test.setTrackerId(trackerID.getText());
        test.setTitle(testTitle.getText());
        test.setDescription(description.getText());
        test.setMinVersions(minVersions.getText());

        //Last tab is "+ button"
        for (int i = 0; i < versionTabs.getTabCount() - 1; i++)
        {
            VersionTab tab = (VersionTab) versionTabs.getComponentAt(i);
            test.addVersion(tab.getVersion());
        }

        return test;
    }

    public void addSaveListener(EditorSaveEvent event) {
        saveListeners.add(event);
    }

    private void setTabTitleUpdated(final VersionTab tab)
    {
        tab.addVersionListener(new VersionTab.VersionListener() {
            @Override
            public void newVersion(String version) {
                for (int i = 0; i < versionTabs.getTabCount(); i++) {
                    Component c = versionTabs.getComponentAt(i);
                    if (c == tab)
                    {
                        versionTabs.setTitleAt(i, version);
                        break;
                    }
                }
            }
        });
    }
}
