/*
* This file is part of jfbtedit.
* Copyright (C) 2014 Artyom Smirnov
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package org.artyomsmirnov.jfbtedit;

import javax.swing.SwingUtilities;
import java.io.IOException;

public class Main {
    public static void main(final String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                String root = ".";
                if (args.length == 1) {
                    root = args[0];
                } else if (args.length > 1) {
                    System.out.println("Only one argument expected!");
                    System.exit(1);
                }
                try {
                    root = new java.io.File(root).getCanonicalPath();
                } catch (IOException e) {
                    e.printStackTrace();
                    System.exit(1);
                }
                new Explorer(root);
            }
        });
    }
}
