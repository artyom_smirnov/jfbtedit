/*
 * This file is part of jfbtedit.
 * Copyright (C) 2014 Artyom Smirnov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.artyomsmirnov.jfbtedit;

import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;

class FormUpdater implements DocumentListener {
    Container container;

    FormUpdater(Container container) {
        super();
        this.container = container;
    }

    private void updateForm() {
        container.validate();
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        updateForm();
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        updateForm();
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
        updateForm();
    }
}
